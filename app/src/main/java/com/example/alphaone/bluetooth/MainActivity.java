package com.example.alphaone.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;

public class MainActivity extends AppCompatActivity {
Button button,button2,button3,button4;
    TextView t1;
BluetoothAdapter bluetoothAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button)findViewById(R.id.button);
        button2 = (Button)findViewById(R.id.button2);
        button3 = (Button)findViewById(R.id.button3);
        button4 = (Button)findViewById(R.id.button4);
        t1 = (TextView)findViewById(R.id.textView);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (bluetoothAdapter == null)
                {
                    Toast.makeText(getApplicationContext(),"DEVICE NOT SUPPORTED",Toast.LENGTH_LONG).show();
                }
                else
                {
                    if (!bluetoothAdapter.isEnabled())
                    {
                        try {
                            Intent enablebluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivityForResult(enablebluetooth, 512);
                        }catch (Exception e){ Toast.makeText(getApplicationContext(),e.toString()+"",Toast.LENGTH_SHORT).show(); }
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"BLUE TOOTH IS ON",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bluetoothAdapter.isEnabled())
                {
                    try {
                        Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();
                        for (BluetoothDevice device : devices) {
                            t1.append("\n DEVICE NAME" + device.getName() + " ADDRESS" + device.getAddress());
                        }
                    }catch (Exception e){Toast.makeText(getApplicationContext(),e.toString()+"",Toast.LENGTH_LONG).show();}
                }
                else { Toast.makeText(getApplicationContext(),"ENABLE BLUETOOTH",Toast.LENGTH_LONG).show(); }
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bluetoothAdapter.disable();
                Toast.makeText(getApplicationContext(),"BLUETOOTH TURNED OFF",Toast.LENGTH_LONG).show();
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentFilter filter = new IntentFilter();
                filter.addAction(BluetoothDevice.ACTION_FOUND);
                registerReceiver(broadcast,filter);
                bluetoothAdapter.startDiscovery();
            }
        });}

        private final BroadcastReceiver broadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (BluetoothDevice.ACTION_FOUND.equals(action))
                {
                    BluetoothDevice bluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if (bluetoothDevice.getBondState()!=BluetoothDevice.BOND_BONDED)
                    {
                        t1.append("\n"+bluetoothDevice.getName()+"  "+bluetoothDevice.getAddress());
                    }
                }
            }

        };

    public void onActivityResult(int requestcode, int resultcode, Intent data)
    {
        if (requestcode == resultcode)
        {
            if (resultcode == RESULT_OK)
            {
                Toast.makeText(getApplicationContext(),"BLUE TOOTH IS TURNED ON",Toast.LENGTH_LONG).show();
            }
            else if(resultcode == RESULT_CANCELED)
            {
                Toast.makeText(getApplicationContext(),"REQUEST DENIED",Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
